---
version: 2
titre: Simulateur 3D de ballon à air chaud
type de projet: Projet de bachelor
année scolaire: 2020/2021
abréviation: 3D Ballons
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Richard Baltensperger
mots-clé: [moteur jeu 3D, modèle de la physique des ballons, casque de réalité virtuelle]
langue: [F]
confidentialité: non
suite: oui
---

\begin{center}
\includegraphics[width=0.7\textwidth]{img/ballon.jpg}
\end{center}

## Contexte/Objectifs
Espace Ballon (https://www.espace-ballon.ch) est une fondation située à Château d’Oex dont l’objectif est de promouvoir les ballons à air chaud ainsi que la région. 

Pour augmenter leur offre d’activités dans leurs locaux, Espace Ballon désire disposer d’un simulateur virtuel 3D de ballon à air chaud de niveau professionnel qui permet au public de s’initier au pilotage et aux professionnels d’effectuer des heures de vol d’entrainement.

Le projet consiste à développer un simulateur 3D de ballon à air chaud. 
Les objectifs sont :

- Développer et intégrer la physique du ballon à air chaud
- Lire et utiliser des fichiers GRIB2 comme source des vents
- Déplacer un ballon au cours du temps en utilisant le modèle physique dans la scène 3D tout en affichant la topographie de la région du Pays-d’Enhaut ; un casque de réalité virtuelle sera utilisé afin d’améliorer la perception du vol


## Tâches
Travaux à réaliser:

- Elaborer le cahier des charges en étroite collaboration avec les responsables du projet
- Développer la physique du ballon à air chaud
- Définir le fonctionnement et le graphisme du simulateur en y intégrant le casque de réalité virtuelle
- Développer le simulateur 3D de ballons à air chaud qui utilise le modèle physique et les vents lus dans les fichiers GRIB2 et affiche la topographie de la région du Pays-d’Enhaut 
- Elaborer une documentation technique complète et rédaction d’un rapport décrivant la démarche et les choix opérés.
