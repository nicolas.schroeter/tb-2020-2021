---
version: 2
titre: Simulateur 3D de vents dans une vallée
type de projet: Projet de bachelor
année scolaire: 2020/2021
abréviation: 3D Vents
filières:
  - Télécommunications
  - Informatique
nombre d'étudiants: 1
professeurs co-superviseurs:
  - Richard Baltensperger
mots-clé: [moteur jeu 3D, modélisation de vents, casque de réalité virtuelle]
langue: [F]
confidentialité: non
suite: oui
---

\begin{center}
\includegraphics[width=0.5\textwidth]{img/espaceballon.jpg}
\end{center}

## Contexte/Objectifs
Espace Ballon (https://www.espace-ballon.ch) est une fondation située à Château d’Oex dont l’objectif est de promouvoir les ballons à air chaud ainsi que la région. 

Pour augmenter leur offre d’activités dans leurs locaux, Espace Ballon désire disposer d’un simulateur virtuel 3D de ballon à air chaud de niveau professionnel qui permet au public de s’initier au pilotage et aux professionnels d’effectuer des heures de vol d’entrainement.

Pour atteindre une qualité professionnelle du simulateur, il est nécessaire de disposer de modélisations très réalistes des différentes situations des vents dans la vallée du Pays-d’Enhaut. 

Les objectifs du projet consistent à

- Modéliser des vents réalistes de la région du Pays-d’Enhaut
- Développer un éditeur adéquat de scènes de vents
- Générer et stocker des fichiers GRIB2 qui représentent cette modélisation des vents
- Visualiser, d’un point de vue externe, les vents dans la scène 3D ; un casque de réalité virtuelle sera utilisé afin d’améliorer la perception des vents

Pour ce projet, nous avons accès à un pilote professionnel qui connait précisément les différentes situations de vents de la région.



## Tâches
Travaux à réaliser:

- Elaborer le cahier des charges en étroite collaboration avec les responsables du projet
- Modéliser quelques vents réalistes de la région du Pays-d'Enhaut
- Générer et stocker des fichiers GRIB2 qui représentent cette modélisation des vents
- Définir le fonctionnement et le graphisme du simulateur en y intégrant le casque de réalité virtuelle
- Développer le simulateur qui visualise d'un point de vue externe, les vents dans la scène 3D
- Elaborer une documentation technique complète et rédaction d’un rapport décrivant la démarche et les choix opérés.
